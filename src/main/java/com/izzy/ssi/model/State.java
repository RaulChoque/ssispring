/**
 * @autor: Raúl Choque
 **/
package com.izzy.ssi.model;

import javax.persistence.Entity;

@Entity

public class State extends ModelBase {
    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
