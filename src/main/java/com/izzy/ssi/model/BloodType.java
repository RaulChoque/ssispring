/**
 * @autor: Raúl Choque
 **/
package com.izzy.ssi.model;

import javax.persistence.Entity;

@Entity

public class BloodType extends ModelBase {
    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
