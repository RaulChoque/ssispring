/**
 * @autor: Raúl Choque
 **/
package com.izzy.ssi.model;

import javax.persistence.Entity;
import javax.persistence.OneToOne;

@Entity

public class Profile extends ModelBase {
    private String name;
    private Integer yearsOfExperience;
    @OneToOne(optional = false)
    private Employee employee;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getYearsOfExperience() {
        return yearsOfExperience;
    }

    public void setYearsOfExperience(Integer yearsOfExperience) {
        this.yearsOfExperience = yearsOfExperience;
    }

    public Employee getEmployee() {
        return employee;
    }

    public void setEmployee(Employee employee) {
        this.employee = employee;
    }
}
