/**
 * @author: raulchoque :)
 **/
package com.izzy.ssi.repositories;

import org.springframework.data.repository.CrudRepository;
import com.izzy.ssi.model.Employee;

public interface EmployeeRepository extends CrudRepository<Employee, Long> {
}
