/**
 * @author: raulchoque :)
 **/
package com.izzy.ssi.repositories;

import org.springframework.data.repository.CrudRepository;
import com.izzy.ssi.model.Profile;

public interface ProfileRepository extends CrudRepository<Profile, Long> {
}
