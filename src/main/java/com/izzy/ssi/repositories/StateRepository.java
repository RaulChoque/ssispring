/**
 * @author: raulchoque :)
 **/
package com.izzy.ssi.repositories;

import org.springframework.data.repository.CrudRepository;
import com.izzy.ssi.model.State;

public interface StateRepository extends CrudRepository<State, Long> {
}
