/**
 * @author: raulchoque :)
 **/
package com.izzy.ssi.repositories;

import org.springframework.data.repository.CrudRepository;
import com.izzy.ssi.model.BloodType;

public interface BloodTypeRepository extends CrudRepository<BloodType, Long> {
}
