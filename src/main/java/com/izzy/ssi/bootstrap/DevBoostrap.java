/**
 * @autor: Raúl Choque
 **/
package com.izzy.ssi.bootstrap;

import com.izzy.ssi.model.BloodType;
import com.izzy.ssi.model.Employee;
import com.izzy.ssi.model.Profile;
import com.izzy.ssi.model.State;
import com.izzy.ssi.repositories.BloodTypeRepository;
import com.izzy.ssi.repositories.EmployeeRepository;
import com.izzy.ssi.repositories.ProfileRepository;
import com.izzy.ssi.repositories.StateRepository;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Component

public class DevBoostrap implements ApplicationListener<ContextRefreshedEvent> {

    private BloodTypeRepository bloodTypeRepository;
    private EmployeeRepository employeeRepository;
    private ProfileRepository profileRepository;
    private StateRepository stateRepository;

    public DevBoostrap(BloodTypeRepository bloodTypeRepository, EmployeeRepository employeeRepository, ProfileRepository profileRepository, StateRepository stateRepository) {
        this.bloodTypeRepository = bloodTypeRepository;
        this.employeeRepository = employeeRepository;
        this.profileRepository = profileRepository;
        this.stateRepository = stateRepository;
    }

    @Override
    public void onApplicationEvent(ContextRefreshedEvent applicationEvent) {
        initData();
    }

    private void initData() {
        //bloodType

        BloodType aPositive = new BloodType();
        aPositive.setName("A positivo");
        BloodType aNegative = new BloodType();
        aNegative.setName("A nagativo");
        BloodType bPositive = new BloodType();
        bPositive.setName("B positivo");
        BloodType bNegative = new BloodType();
        bNegative.setName("B negativo");
        BloodType abPositive = new BloodType();
        abPositive.setName("AB positivo");
        BloodType abNegative = new BloodType();
        abNegative.setName("AB nagativo");
        BloodType oPositive = new BloodType();
        oPositive.setName("O positivo");
        BloodType oNegative = new BloodType();
        oNegative.setName("O negativo");
        bloodTypeRepository.save(aPositive);
        bloodTypeRepository.save(aNegative);
        bloodTypeRepository.save(bPositive);
        bloodTypeRepository.save(bNegative);
        bloodTypeRepository.save(abPositive);
        bloodTypeRepository.save(abNegative);
        bloodTypeRepository.save(oPositive);
        bloodTypeRepository.save(oNegative);


        //state
        State leisure = new State();
        leisure.setName("Ocio");
        State working = new State();
        working.setName("Trabajando");
        State lowMedical = new State();
        lowMedical.setName("Baja Medica");
        stateRepository.save(leisure);
        stateRepository.save(working);
        stateRepository.save(lowMedical);

        //employee
        Employee jose = new Employee();
        jose.setName("Juan Jose");
        jose.setLastName("Garcia Lineras");
        jose.setAge(23);
        jose.setBloodType(abNegative);
        jose.setState(working);
        Employee mario = new Employee();
        mario.setName("Mario");
        mario.setLastName("Fuentes");
        mario.setAge(19);
        mario.setBloodType(abPositive);
        mario.setState(leisure);

        //profile
        Profile architect = new Profile();
        architect.setName("Arquitecto");
        architect.setEmployee(jose);
        architect.setYearsOfExperience(8);
        Profile bricklayer = new Profile();
        bricklayer.setName("Albañil");
        bricklayer.setEmployee(jose);
        bricklayer.setYearsOfExperience(3);
        Profile driver = new Profile();
        driver.setName("Conductor");
        driver.setEmployee(mario);
        driver.setYearsOfExperience(15);


        jose.getProfile().add(architect);
        jose.getProfile().add(bricklayer);
        employeeRepository.save(jose);
        profileRepository.save(architect);

        mario.getProfile().add(driver);
        employeeRepository.save(mario);
        profileRepository.save(driver);


    }
}
